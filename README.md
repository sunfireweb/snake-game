# snake-game
A browser-based game, using the HTML5 `<canvas>` element through Javascript to draw the snake and respond to user keystrokes.

High score comes from the browser's LocalStorage, for now. Scoreboard branch is for development of a global top 10 feature.

Not mobile-friendly.


## Changelog
Originally created and published on sunfireweb.com in March 2015.  
Start/Pause functionality added May 2017.  
Republished on http://hitsaru.com/snake January 2019.
